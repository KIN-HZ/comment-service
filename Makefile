all:
	cp .env.template .env && docker-compose build

start:
	docker-compose up

shell:
	docker-compose run --rm app /bin/bash

node-shell:
	docker-compose run --rm nodejs /bin/sh

npm-install:
	docker-compose run --rm nodejs npm install

export-packages:
	docker-compose run --rm app poetry export --output requirements.prod.txt

export-dev-packages:
	docker-compose run --rm app poetry export \
			--output requirements.dev.txt --dev

make-migrations:
	docker-compose run --rm app python manage.py makemigrations

migrate:
	docker-compose run --rm app python manage.py migrate

reset-db:
	docker-compose run --rm app python manage.py reset_db

from django.db.models import Manager, Exists, OuterRef, QuerySet, F


class CommentManager(Manager):
    """Manager of the Comment Model"""

    def get_queryset(self) -> QuerySet:
        """Set author username to global annotation"""
        return super().get_queryset().annotate(author=F('user__username'))

    def get_grand_parents(self) -> QuerySet:
        """
        :return: Comments of the highest level with `child_exists` flag which
        shows has comment child nodes or not
        """
        exists_qs = Exists(self.model.objects.filter(parent=OuterRef('pk')))

        return self.filter(parent__isnull=True).annotate(
            descendant_exists=exists_qs).select_related('user')

    def get_descendants(self, ancestor_id: int, include_self: bool = False) \
            -> QuerySet:
        """
        :param ancestor_id: id of comment
        :param include_self: flag which defines include self object to result
        or not
        :return: All child nodes
        """
        queryset = self.annotate(closure_parent__child_id=ancestor_id)

        if not include_self:
            queryset = queryset.exclude(pk=ancestor_id)

        return queryset

from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from ..models import Comment, CommentClosureTree
from ..exceptions import CommentException


@receiver(pre_delete, sender=Comment)
def delete_comments_tree_handler(_, **kwargs):
    """Pre-delete signal used to disallow deleting
    comment which has children nodes"""
    if Comment.objects.filter(parent=kwargs['instance'].pk).exists():
        raise CommentException


@receiver(post_save, sender=Comment)
def create_comments_tree_handler(_, **kwargs) -> None:
    """Create link for closure tree"""
    instance = kwargs['instance']

    if kwargs['created']:
        to_save = [CommentClosureTree(
            parent_id=instance.pk,
            child_id=instance.pk,
            depth=0
        )]

        if instance.parent_id:
            nodes = CommentClosureTree.objects.select_related(
                'ancestor').filter(descendant=instance.parent)

            for row in nodes:
                to_save.append(CommentClosureTree(
                    parent_id=row.parent_id,
                    child_id=instance.pk,
                    depth=row.depth + 1
                ))

        CommentClosureTree.objects.bulk_create(to_save)

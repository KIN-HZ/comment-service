class CommentException(Exception):
    message = 'Could not to delete a comment, because it has children nodes'

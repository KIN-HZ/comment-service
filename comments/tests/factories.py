import factory

from ..models import Comment


class CommentFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Comment

    body = factory.Faker('text')
    parent = factory.SubFactory('comments.tests.factories.CommentFactory')
    author = factory.SubFactory('accounts.tests.factories.UserFactory')
    content_type = None
    object_id = None

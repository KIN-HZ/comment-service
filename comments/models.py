from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.db import models

from core.abstract_models import DateTimeAbstractModel


class Comment(DateTimeAbstractModel):
    """Model containing comments of different entities
    such as Article, Blog etc."""

    body = models.TextField(verbose_name='The text of the comment')
    parent = models.ForeignKey('self', related_name='children',
                               verbose_name='Nearest Parent comment',
                               on_delete=models.CASCADE, null=True)
    author = models.ForeignKey(User, verbose_name='Comment author',
                               on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                     verbose_name='Type of entity')
    content_object = GenericForeignKey('content_type', 'object_id')
    object_id = models.PositiveIntegerField(verbose_name='Entity ID')

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'

    def __str__(self) -> str:
        if len(self.body) >= 50:
            return self.body[0:50] + '...'

        return self.body

    @property
    def has_descendants(self) -> bool:
        """Check if object has children nodes"""
        if hasattr(self, '_cached_descendants'):
            return True

        return getattr(self, 'descendant_exists', False)

    @property
    def descendants(self) -> list:
        return getattr(self, '_cached_descendants', [])


class CommentClosureTree(models.Model):
    """The Relationship model used to implement the `Closure Table` pattern"""

    parent = models.ForeignKey(Comment, related_name='closure_children',
                               verbose_name='Link to parent comment',
                               on_delete=models.CASCADE)
    child = models.ForeignKey(Comment, related_name='closure_parent',
                              verbose_name='Link to child comment',
                              on_delete=models.CASCADE)
    level = models.PositiveIntegerField(verbose_name='Level of comment')

    class Meta:
        unique_together = ('parent', 'child')

    def __str__(self) -> str:
        return f"Closure from {self.parent} to {self.child}"

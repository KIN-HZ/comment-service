from core.settings import env


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env.str('DJANGO_DB_NAME'),
        'USER': env.str('DJANGO_DB_USER'),
        'PASSWORD': env.str('DJANGO_DB_PASSWORD'),
        'HOST': env.str('DJANGO_DB_HOST'),
        'PORT': env.int('DJANGO_DB_PORT')
    }
}

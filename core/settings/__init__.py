from pathlib import Path

import environ
from split_settings.tools import include


BASE_DIR = Path(__file__).resolve().parent.parent.parent

env = environ.Env()
environ.Env.read_env(str(BASE_DIR / '.env'))

include(
    'common.py',
    'database.py',
    'auth.py',
    'static.py',
    'media.py'
)

from django.db.models import Model, DateTimeField


class DateTimeAbstractModel(Model):
    """Abstract model used to add timestamps of object
    creation and last update of it"""

    created_at = DateTimeField(auto_now_add=True,
                               verbose_name='Date and time of entity creation')
    updated_at = DateTimeField(auto_now=True,
                               verbose_name='Date and time of last '
                                            'entity update')

    class Meta:
        abstract = True
        ordering = ['-created_at']

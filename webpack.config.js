'use strict';

const path = require('path');
const miniCss = require('mini-css-extract-plugin');

module.exports = {
  entry: ['./static/js/app.js'],
  output: {
    filename: 'app.bundle.js',
    path: __dirname + '/static/dist',
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [miniCss.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/preset-env',
                {
                  debug: false,
                  corejs: 3,
                  useBuiltIns: 'usage',
                },
              ],
            ],
          },
        },
      },
    ],
  },
  plugins: [
    new miniCss({
      filename: 'style.css',
    }),
  ],
};

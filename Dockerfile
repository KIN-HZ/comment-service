FROM python:3.9.4

ENV PYTHONUNBUFFERED=1

WORKDIR /app

COPY requirements.dev.txt .

RUN pip install --upgrade pip
RUN pip install poetry
RUN pip install -r requirements.dev.txt

COPY . .

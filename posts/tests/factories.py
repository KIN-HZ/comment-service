import factory

from ..models import Blog, Article


class BlogFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Blog

    title = factory.Faker('word')
    body = factory.Faker('text')


class ArticleFactory(BlogFactory):

    class Meta:
        model = Article

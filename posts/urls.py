from django.urls import path, include

from .views import PostListView, PostDetailView
from .models import Blog, Article

app_name = 'posts'

urlpatterns = [
    path('blogs/', include(([
        path('', PostListView.as_view(model=Blog, title='Blogs'),
             name='index'),
        path('show/<int:pk>/', PostDetailView.as_view(model=Blog),
             name='detail'),
    ], 'blogs'))),
    path('articles/', include(([
        path('', PostListView.as_view(model=Article, title='Articles'),
             name='index'),
        path('show/<int:pk>/', PostDetailView.as_view(model=Article),
             name='detail'),
    ], 'articles'))),
]

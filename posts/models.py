from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.urls import reverse

from core.abstract_models import DateTimeAbstractModel
from comments.models import Comment


class Post(DateTimeAbstractModel, models.Model):
    """Abstract model used to implement texting models
    such as Blog, Article etc."""

    BRIEF_LENGTH = 150

    title = models.CharField(verbose_name="Post title", max_length=255)
    body = models.TextField(verbose_name="Post description")
    comments = GenericRelation(Comment)

    class Meta:
        abstract = True
        ordering = DateTimeAbstractModel.Meta.ordering

    def __str__(self) -> str:
        return self.title

    def get_absolut_url(self) -> str:
        """
        :return: url to detail page
        """
        return reverse(f'{self.model_name}:detail', args=[self.pk])

    @property
    def brief(self) -> str:
        """
        :return: A brief part of the text of the post
        """
        if len(self.body) > self.BRIEF_LENGTH:
            return self.body[0:self.BRIEF_LENGTH] + '...'
        return self.body

    @property
    def model_name(self) -> str:
        """
        :return: Model name
        """
        return self._meta.model_name

    @property
    def model_name_plural(self) -> str:
        """
        :return: Model name in the plural
        """
        return self.model_name + 's'


class Blog(Post):
    """The Model containing blog's posts"""
    ...


class Article(Post):
    """The Model containing news articles"""
    ...

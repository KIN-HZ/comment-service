from typing import Union, Type

from django.views.generic import ListView, DetailView, TemplateView
from django.views.generic.list import MultipleObjectMixin
from django.db.models import QuerySet

from .models import Blog, Article
from .consts import POSTS_LIMIT, COMMENTS_LIMIT


class HomeView(TemplateView):
    """Project home page"""

    template_name = 'posts/index.html'
    page_title = 'Home Page'

    def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data(**kwargs)
        context['blogs'] = self.get_queryset(Blog)
        context['articles'] = self.get_queryset(Article)
        context['title'] = 'Home page'

        return context

    @staticmethod
    def get_queryset(model: Union[Type[Blog], Type[Article]]) \
            -> QuerySet[Blog, Article]:
        """
        :return: Last ten records from the passed model
        """
        return model.objects.all()[0:POSTS_LIMIT]


class PostListView(ListView):
    """List of posts from with pagination"""

    template_name = 'posts/list.html'
    paginate_by = POSTS_LIMIT
    title = None

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['title'] = getattr(self, 'title', 'Posts')

        return context


class PostDetailView(DetailView, MultipleObjectMixin):
    """Post single page"""

    template_name = 'posts/detail.html'
    comments_paginate_by = COMMENTS_LIMIT

    def get_comments_queryset(self) -> QuerySet:
        """
        :return: Post's comments
        """
        return self.object.comments.get_grand_parents()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        """Get the post's comments with pagination"""
        paginator, page, comments, is_paginated = self.paginate_queryset(
            self.get_comments_queryset(), self.comments_paginate_by)

        context['paginator'] = paginator
        context['page_obj'] = page
        context['is_paginated'] = is_paginated
        context['comments'] = comments

        return context

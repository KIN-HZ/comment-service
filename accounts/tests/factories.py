import factory
from django.contrib.auth.models import User


class UserFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = User

    email = factory.Faker('safe_email')
    username = factory.Faker('name')
    password = factory.PostGenerationMethodCall('set_password', 'qwerty123')
